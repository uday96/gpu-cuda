import sys

if(len(sys.argv)!=2):
	print "Usage : python refine.py <filename>"

fname = sys.argv[1]

f = open(fname,'r')

vals = f.readline()[:-1].split(" ")
n = int(vals[0])
m = int(vals[1])

print n, m

fw = open("input.txt",'w')
fw.write(str(n)+"\t"+str(m)+"\n")

edges = {}

for k in range(2*m):
	l = f.readline()
	if(l is None):
		break
	if(l[-1] == '\n'):
		l = l[:-1]
	l = l.split("\t")
	a = l[0]
	b = l[1]
	s1 = str(a)+"-"+str(b)
	s2 = str(b)+"-"+str(a)
	if((not edges.has_key(s1)) and (not edges.has_key(s2))):
		edges[s1] = True
		fw.write(str(a)+"\t"+str(b)+"\n")

f.close()
fw.close()