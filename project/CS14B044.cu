#include <stdio.h>
#include <iostream>
#include <fstream>
#include "timer.h"
#include "functions.h"
using namespace std;

__global__ void initWorkList(int *Win, int n) {
    int v = blockIdx.x * blockDim.x + threadIdx.x;
    if(v >= n){
        return;
    }
    Win[v] = v;
}

__global__ void TopoFirstFit(int *degree_prefix_sum, int *neighbors, int n, int m, int *color, bool *changed) {
    int v = blockIdx.x * blockDim.x + threadIdx.x;
    if(v >= n){
        return;
    }
    if(color[v] == 0){
        int prevDegSum = 0;
        int currDegSum = degree_prefix_sum[v];
        if(v > 0){
            prevDegSum = degree_prefix_sum[v-1];
        }
        int colorMask[MAXCOLOR];
        memset(colorMask, -1, MAXCOLOR * sizeof(int));
        //printf("ff : %d - %d : %d %d\n", v, color[v], prevDegSum, currDegSum);
        for(int index = prevDegSum; index< currDegSum; index++){
            int neighbor = neighbors[index];
            colorMask[color[neighbor]] = v;        
        }
        int minColor = 0;
        for(minColor = 0; minColor < MAXCOLOR; minColor++){
            if(colorMask[minColor] == -1 && minColor > 0){
                break;
            }
        }
        color[v] = minColor;
        //printf("firstFit : %d - %d\n", v, color[v]);
        *changed = true;
    }
}

__global__ void TopoConflictResolve(int *degree_prefix_sum, int *neighbors, int n, int m, int *color, bool *colored) {
    int v = blockIdx.x * blockDim.x + threadIdx.x;
    if(v >= n){
        return;
    }
    if(colored[v] == false){
        int prevDegSum = 0;
        int currDegSum = degree_prefix_sum[v];
        if(v > 0){
            prevDegSum = degree_prefix_sum[v-1];
        }
        bool isConflict = false;
        for(int index = prevDegSum; index< currDegSum; index++){
            int neighbor = neighbors[index];
            if(color[v] == color[neighbor] && v < neighbor){
                color[v] = 0;
                isConflict = true;
                //printf("conflict : %d - %d\n", v, color[v]);
                break;
            }
        }
        if(isConflict == false){
            colored[v] = true;
            //printf("colored : %d - %d\n", v, color[v]);
        }
    }
}

__global__ void DataFirstFit(int *degree_prefix_sum, int *neighbors, int *Win, int *WinLen, int *color) {
    int id = blockIdx.x * blockDim.x + threadIdx.x;
    if(id >= *WinLen){
        return;
    }
    
    int v = Win[id];
    int prevDegSum = 0;
    int currDegSum = degree_prefix_sum[v];
    if(v > 0){
        prevDegSum = degree_prefix_sum[v-1];
    }
    int colorMask[MAXCOLOR];
    memset(colorMask, -1, MAXCOLOR * sizeof(int));
    //printf("ff : %d - %d : %d %d\n", v, color[v], prevDegSum, currDegSum);
    for(int index = prevDegSum; index< currDegSum; index++){
        int neighbor = neighbors[index];
        colorMask[color[neighbor]] = v;        
    }
    int minColor = 0;
    for(minColor = 0; minColor < MAXCOLOR; minColor++){
        if(colorMask[minColor] == -1 && minColor > 0){
            break;
        }
    }
    color[v] = minColor;
    //printf("firstFit : %d - %d\n", v, color[v]);
}

__global__ void DataConflictResolve(int *degree_prefix_sum, int *neighbors, int *Win, int *WinLen, int *Wout, int *WoutIndex, int *color) {
    int id = blockIdx.x * blockDim.x + threadIdx.x;
    if(id >= *WinLen){
        return;
    }
    int v = Win[id];
    int prevDegSum = 0;
    int currDegSum = degree_prefix_sum[v];
    if(v > 0){
        prevDegSum = degree_prefix_sum[v-1];
    }
    bool isConflict = false;
    for(int index = prevDegSum; index< currDegSum; index++){
        int neighbor = neighbors[index];
        if(color[v] == color[neighbor] && v < neighbor){
            color[v] = 0;
            isConflict = true;
            break;
        }
    }
    if(isConflict == true){
        int oldIndex = atomicAdd(WoutIndex,1);
        Wout[oldIndex] = v;
        //printf("conflict : %d - %d\n", v, color[v]);
    }
}

int main() {
    int n = -1, m=-1;

    cin >> n;
    cin >> m;
    
    int NUMTHREADS = 1024;
    if(NUMTHREADS > n){
        NUMTHREADS = n;
    }
    int NUMBLOCKS = ((n - 1) / NUMTHREADS + 1);

    int *degree_prefix_sum_host = (int *) malloc(n * sizeof(int));
    int *neighbors_host = (int *) malloc(m * 2 * sizeof(int));
    
    readGraph(degree_prefix_sum_host, neighbors_host, n, m);

    sequentialGraphColoring(degree_prefix_sum_host, neighbors_host, n, m);

    int *degree_prefix_sum_device, *neighbors_device;
    cudaMalloc(&degree_prefix_sum_device, n * sizeof(int));
    cudaMalloc(&neighbors_device, m * 2 * sizeof(int));
    cudaMemcpy(degree_prefix_sum_device, degree_prefix_sum_host, n * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(neighbors_device, neighbors_host, m * 2 * sizeof(int), cudaMemcpyHostToDevice);

    int *color;
    cudaMalloc(&color, n * sizeof(int));
    cudaMemset(color, 0, n * sizeof(int));

    GPUTimer topotimer, datatimer;

    bool *colored;
    cudaMalloc(&colored, n * sizeof(bool));
    cudaMemset(colored, false, n * sizeof(bool));

    bool changed_host = false;
    bool *changed_device;
    cudaMalloc(&changed_device, sizeof(bool));

    topotimer.Start();

    do{
        changed_host = false;
        cudaMemcpy(changed_device, &changed_host, sizeof(changed_host), cudaMemcpyHostToDevice);
        
        TopoFirstFit<<<NUMBLOCKS, NUMTHREADS>>> (degree_prefix_sum_device, neighbors_device, n, m, color, changed_device);
        
        cudaMemcpy(&changed_host, changed_device, sizeof(changed_host), cudaMemcpyDeviceToHost);
        
        TopoConflictResolve<<<NUMBLOCKS, NUMTHREADS>>> (degree_prefix_sum_device, neighbors_device, n, m, color, colored);
        
        cudaDeviceSynchronize();
        // if(changed_host){
        //     cout<<"-----changed-----"<<endl;
        // }
    }while( changed_host == true );

    cudaDeviceSynchronize();

    topotimer.Stop();

    int *finalColor = (int *)malloc(n * sizeof(int));
    cudaMemcpy(finalColor, color, n * sizeof(int), cudaMemcpyDeviceToHost);

    cout<<endl;
    cout<<"--------------------------------------"<<endl;
    cout<<"Parallel Graph Coloring - Topo Driven"<<endl;
    cout<<"--------------------------------------"<<endl;

    int numColors = colorsUsed(finalColor, n);
    cout<<"#Nodes : "<<n<<endl;
    cout<<"#Edges : "<<m<<endl;
    cout<<"#Colors : "<<numColors<<endl;
    printf("Result : %s\n",CheckSolution(degree_prefix_sum_host, neighbors_host, n, finalColor) ? "Success" : "Failure");
    printf("GPU Code Exec Time : %f ms\n", topotimer.Elapsed());
    
    char tfname[] = "output_topo.txt";
    writeSolution(finalColor, n, m, numColors, tfname);
    
    int *Win;
    cudaMalloc(&Win, n * sizeof(int));
    
    int hWinLen = n;
    int *dWinLen;
    cudaMalloc(&dWinLen, sizeof(int));

    int *Wout;
    cudaMalloc(&Wout, n * sizeof(int));

    int *dWoutIndex;
    cudaMalloc(&dWoutIndex, sizeof(int));

    datatimer.Start();

    initWorkList<<<NUMBLOCKS, NUMTHREADS>>> (Win, n);
    cudaDeviceSynchronize();

    while(hWinLen > 0){
        
        NUMTHREADS = 1024;
        if(NUMTHREADS > hWinLen){
            NUMTHREADS = hWinLen;
        }
        NUMBLOCKS = ((hWinLen - 1) / NUMTHREADS + 1);

        cudaMemcpy(dWinLen, &hWinLen, sizeof(hWinLen), cudaMemcpyHostToDevice);
        
        DataFirstFit<<<NUMBLOCKS, NUMTHREADS>>> (degree_prefix_sum_device, neighbors_device, Win, dWinLen, color);
        cudaDeviceSynchronize();
        
        cudaMemset(Wout, -1, n * sizeof(int));
        cudaMemset(dWoutIndex, 0, sizeof(int));
        
        DataConflictResolve<<<NUMBLOCKS, NUMTHREADS>>> (degree_prefix_sum_device, neighbors_device, Win, dWinLen, Wout, dWoutIndex, color);
        
        cudaMemcpy(&hWinLen, dWoutIndex, sizeof(hWinLen), cudaMemcpyDeviceToHost);
        swap(Win,Wout);
    }

    cudaDeviceSynchronize();

    datatimer.Stop();

    cudaMemcpy(finalColor, color, n * sizeof(int), cudaMemcpyDeviceToHost);

    cout<<endl;
    cout<<"--------------------------------------"<<endl;
    cout<<"Parallel Graph Coloring - Data Driven"<<endl;
    cout<<"--------------------------------------"<<endl;

    numColors = colorsUsed(finalColor, n);
    cout<<"#Nodes : "<<n<<endl;
    cout<<"#Edges : "<<m<<endl;
    cout<<"#Colors : "<<numColors<<endl;
    printf("Result : %s\n",CheckSolution(degree_prefix_sum_host, neighbors_host, n, finalColor) ? "Success" : "Failure");
    printf("GPU Code Exec Time : %f ms\n", datatimer.Elapsed());
    
    char dfname[] = "output_data.txt";
    writeSolution(finalColor, n, m, numColors, dfname);

    return 0;
}