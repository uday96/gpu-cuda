#include "timer.h"
#include <thrust/sort.h>
using namespace std;

#define MAXCOLOR 128

void swap(int* &a, int* &b){
  int *temp = a;
  a = b;
  b = temp;
}

void readGraph(int *degree_prefix_sum, int *neighbors, int n, int m){
    
    int *src = (int *)malloc(m * 2 * sizeof(int));
    int *dst = (int *)malloc(m * 2 * sizeof(int));

    int i = 0;
    for(int line=0;line<m;line++){
        int v1,v2;
        cin >> v1;
        cin >> v2;
        src[i] = v1;
        dst[i] = v2;
        i++;
        src[i] = v2;
        dst[i] = v1;
        i++;
    }
    
    thrust::sort_by_key(src, src + m*2, dst);

    // for(int k=0;k<2*m;k++){
    //     cout<<src[k]<<" - "<<dst[k]<<endl;
    // }
    // printf("\n");
    
    int prevVertex = 0;
    int perVertexEdges = 0;
    for(int ind=0;ind<2*m;ind++){
        int v1, v2;
        v1 = src[ind];
        v2 = dst[ind];
        neighbors[ind] = v2;
        if(v1 == prevVertex){            
            perVertexEdges++;
        }
        else{
            degree_prefix_sum[prevVertex] = perVertexEdges;
            if(prevVertex > 0){
                degree_prefix_sum[prevVertex] += degree_prefix_sum[prevVertex-1];  
            }            
            perVertexEdges = 1;
            prevVertex = v1;
        }
    }
    degree_prefix_sum[prevVertex] = perVertexEdges;
    if(prevVertex > 0){
        degree_prefix_sum[prevVertex] += degree_prefix_sum[prevVertex-1];
    }

    free(src);
    free(dst);

    // cout<<n<<" - "<<m<<endl;
    // for(int k=0;k<n;k++){
    //     printf("%d ",degree_prefix_sum[k]);
    // }
    // printf("\n");
    // for(int k=0;k<2*m;k++){
    //     printf("%d ",neighbors[k]);
    // }
    // printf("\n");
}

bool CheckSolution(int *degree_prefix_sum, int *neighbors, int n, int *color){
    for(int v=0;v<n;v++){
        int prevDegSum = 0;
        int currDegSum = degree_prefix_sum[v];
        if(v > 0){
            prevDegSum = degree_prefix_sum[v-1];
        }
        //cout<<"Vertex "<<v<<endl;
        for(int index = prevDegSum; index< currDegSum; index++){
                int neighbor = neighbors[index];
                //cout<<"neighbor "<<neighbor<<endl;
                if(color[v] == color[neighbor]){
                    return false;
                }
        }
    }
    return true;
}

void writeSolution(int *color, int n, int m, int colorsUsed, char *fname){
    ofstream outputFile;
    outputFile.open (fname);
    outputFile<<"#Nodes : "<<n<<endl;
    outputFile<<"#Edges : "<<m<<endl;
    outputFile<<"#Colors : "<<colorsUsed<<"\n"<<endl;
    outputFile<<"Vertex\tColor"<<endl;
    for(int v=0;v<n;v++){
        outputFile<<v<<"\t\t"<<color[v]<<endl;
    }
    outputFile.close();
}

int colorsUsed(int *color, int n){
    int colorMask[MAXCOLOR];
    memset(colorMask, 0, MAXCOLOR * sizeof(int));
    bool colorsUsed[MAXCOLOR];
    memset(colorsUsed, false, MAXCOLOR * sizeof(bool));
    int totalColorsUsed = 0;
    for(int v=0;v<n;v++){
        colorMask[color[v]]++;
        if(!colorsUsed[color[v]]){
        	colorsUsed[color[v]] = true;
        	totalColorsUsed++;
        }
    }
    return totalColorsUsed;
}

void sequentialGraphColoring(int *degree_prefix_sum, int *neighbors, int n, int m){
	int *color = (int *)malloc(n * sizeof(int));
    memset(color, 0, n * sizeof(int));

    CPUTimer cputimer;
    cputimer.Start();

    for(int v=0;v<n;v++){
    	int prevDegSum = 0;
        int currDegSum = degree_prefix_sum[v];
        if(v > 0){
            prevDegSum = degree_prefix_sum[v-1];
        }

        int colorMask[MAXCOLOR];
        memset(colorMask, -1, MAXCOLOR * sizeof(int));

        for(int index = prevDegSum; index< currDegSum; index++){
            int neighbor = neighbors[index];
            colorMask[color[neighbor]] = v;        
        }

        int minColor = 0;
        for(minColor = 0; minColor < MAXCOLOR; minColor++){
            if(colorMask[minColor] == -1 && minColor > 0){
                break;
            }
        }

        color[v] = minColor;
    }

    cputimer.Stop();

    int numColors = colorsUsed(color, n);

    cout<<"--------------------------------------"<<endl;
    cout<<"Sequential Graph Coloring"<<endl;
    cout<<"--------------------------------------"<<endl;
    cout<<"#Nodes : "<<n<<endl;
    cout<<"#Edges : "<<m<<endl;
    cout<<"#Colors : "<<numColors<<endl;
    printf("Result : %s\n",CheckSolution(degree_prefix_sum, neighbors, n, color) ? "Success" : "Failure");
    printf("CPU Code Exec Time : %f ms\n", cputimer.Elapsed());

    char fname[] = "output_seq.txt";
    writeSolution(color, n, m, numColors, fname);
}