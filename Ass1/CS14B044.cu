#include <cuda_runtime.h>
#include "kernels.h"

__global__ void transpose_parallel_per_row(float *in, float *out, int rows_in, int cols_in)
{
  // TODO: Write your implementation here 
  int rowID = (blockIdx.x*blockDim.x)+threadIdx.x;
  int colID = 0;
  if(rowID < rows_in){
  	for(colID=0;colID<cols_in;colID++){
  		out[(colID*rows_in)+rowID] = in[(rowID*cols_in)+colID];
  	}
  }
}

__global__ void 
transpose_parallel_per_element(float *in, float *out, int rows_in, int cols_in, int K1, int K2)
{
  // TODO: Write your implementation here 
  int rowID = (blockIdx.x*blockDim.x)+threadIdx.x;
  int colID = (blockIdx.y*blockDim.y)+threadIdx.y;
  if(rowID < rows_in && colID < cols_in){
  	out[(colID*rows_in)+rowID] = in[(rowID*cols_in)+colID];
  }
}
