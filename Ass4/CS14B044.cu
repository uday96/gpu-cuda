#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/find.h>
#include <thrust/transform.h>
#include <thrust/sequence.h>
//#include <thrust/copy.h>
//#include <iostream>
using namespace std;

struct greater_than{
  __host__ __device__ bool operator()(int x){
    return x >= 0;
  }
};

struct nqueen_functor {
	const int index,N;
	nqueen_functor(int _index, int _N) : index(_index),N(_N) {}
	__host__ __device__ int operator()(const int& x, const int& y)const {
		if(x == 0 || index == y){
			return -1;
		}
		int row1 = index/N;
		int col1 = index%N;
		int row2 = y/N;
		int col2 = y%N;
		if(row1 == row2 || col1 == col2){
			return y;
		}
		if(row1+col1 == row2+col2 || row1-col1 == row2-col2){
			return y;
		}
		return -1;
	}
};

int main() {
    int N;
    cin >> N;
    thrust::host_vector<int> H_input(N*N);
    thrust::host_vector<int> H_seq(N*N);
    thrust::sequence(H_seq.begin(), H_seq.end());
    thrust::host_vector<int> H_indices(N);
    int index=0;
    for(int i=0;i<N;i++){
    	for(int j=0;j<N;j++){
    		cin >> H_input[i*N+j];
    		//cout << H_input[i*N+j] << " ";
    		if(H_input[i*N+j]){
    			H_indices[index] = i*N+j;
    			index++;
    		}
    	}
    	//cout << "" << endl;
    }
    thrust::device_vector<int> D_input = H_input;
    thrust::device_vector<int> D_indices = H_indices;
    thrust::device_vector<int> D_seq = H_seq;

    thrust::device_vector<int> D_result(N*N);
    thrust::device_vector<int>::iterator iter;

    for( thrust::host_vector<int>::iterator it = H_indices.begin(); it != H_indices.end(); it++ ){
    	//cout << *it << endl;
    	//cout << "-----------------" << endl;
    	thrust::transform(D_input.begin(), D_input.end(), D_seq.begin(), D_result.begin(), nqueen_functor(*it,N));
    	//thrust::copy(D_result.begin(), D_result.end(), std::ostream_iterator<int>(std::cout, "\n"));
		iter = thrust::find_if(D_result.begin(), D_result.end(), greater_than());
		if(iter < D_result.end()){
			cout << "NO" << endl;
			cout << *it/N <<" "<<*it%N << endl;
			cout << *iter/N <<" "<< *iter%N << endl;
			return 0;
		}
		//cout << "=================" << endl;
    }
    cout << "YES" << endl;
    return 0;
}