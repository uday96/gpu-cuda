
#define ARRAY_SIZE 1000000

__device__ volatile bool status[ARRAY_SIZE] = {false};


/* The kernel "msort" should sort the input array using parallel merge-sort. */

__global__ void msort(int *d_input, int* d_temp, int N)
{
	int index = (blockIdx.x*blockDim.x)+threadIdx.x;
	if(index >= N){
		return;
	}
	int levels = (int)ceilf(log2f(N));
	int currLevel = 1;
	for(currLevel=1;currLevel<=levels;currLevel++){
		int toCompare = (int)exp2f(currLevel);
		if(index%toCompare != 0){
			return;
		}
		int i = index;
		int j = index + (int)(toCompare/2);
		int k = index;
		int n1 = index + (int)(toCompare/2);
		int n2 = index + toCompare;
		if(n2>N){
			n2 = N;
		}
		while (i < n1 && j < n2){
	        if (d_input[i] <= d_input[j]){
	            d_temp[k] = d_input[i];
	            i++;
	        }
	        else{
	            d_temp[k] = d_input[j];
	            j++;
	        }
	        k++;
	    }
	    while (i < n1){
	        d_temp[k] = d_input[i];
	        i++;
	        k++;
	    }
	    while (j < n2){
	        d_temp[k] = d_input[j];
	        j++;
	        k++;
	    }
	    for(i=index;i<index+toCompare;i++){
	    	d_input[i] = d_temp[i];
	    }
	    status[index] = true;
	    if(index%(2*toCompare) != 0){
			return;
		}
		if(index+toCompare < N){
			while(!(status[index] && status[index+toCompare])){
				// syncthreads until next comparision
		 	}
		 	status[index] = false;
		 	status[index+toCompare] = false;
		}
		else{
			status[index] = false;
		}
	}
}
