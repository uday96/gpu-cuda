// #include <stdio.h>
// #include "timer.h"
// #include "kernel.h"

// //This is a sample generation of the input matrix. The assignment need not be evaluated on the same matrix.


// int main(int argc, char** argv)
// {
//   // specify the dimensions of the input matrix
//   int K=3;
//   int P=90;
//   int N=K*P;
//   unsigned numbytes =  N * sizeof(int);
//   int *h_input = (int *) malloc(numbytes);
//   int *h_output = (int *) malloc(numbytes);
//   int *d_input, *d_temp;
//   for(int i=0;i<N;i++){
//       h_input[i]=N-i;
//   }
//   cudaError_t err;
//   cudaMalloc(&d_input, numbytes);
//   cudaMalloc(&d_temp, numbytes);
//   cudaMemset(d_temp, 0, numbytes);
//   cudaMemcpy(d_input, h_input, numbytes, cudaMemcpyHostToDevice);
//   GPUTimer timer;
//   timer.Start();
//   msort<<<K,P>>>(d_input, d_temp,N);
//   timer.Stop();
//   err = cudaGetLastError();
//   if (err != cudaSuccess)
//     printf("Error: %s\n", cudaGetErrorString(err));
//   cudaMemcpy(h_output, d_input, numbytes, cudaMemcpyDeviceToHost);
//   for(int i=0;i<N-1;i++){
//       printf("%d ",h_output[i]);
//       if(h_output[i]>h_output[i+1]){
//           printf("failure\n");
//           return 0;
//       }
//   }
//   printf("succes\n");
//   return 0;
// }

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "timer.h"
#include "kernel.h"

#define P printf("line: %d\n", __LINE__)

//This is a sample generation of the input matrix. The assignment need not be evaluated on the same matrix.
 
void fill_array(int *arr, int N)
{
  for(int i=0; i<N; i++)
  {
      arr[i] = N-i;
  }
}

// Print the matrix
void print_array(int *arr, int N)
{
  for(int i=0; i < N; i++) 
  {
    printf("%d ", arr[i]);
  } 
  printf("\n");
}

// Verify the correctness by comparing the sequential output with parallel output
bool compare_arrays(int *gpu, int *ref, int N)
{

  for(int i=0; i < N; i++)
  {
    if (gpu[i] != ref[i]) {
      return false;
    }
  }
    return true; // generated output matches expected output
}

void mergesort(int out[], int temp[], int si, int size)
{
  int l_size = size/2;
  int r_size = size-l_size;

  if (size <= 1) {
    return;
  }

  int li = si, ri = si+l_size;
  mergesort(out, temp, li, l_size);
  mergesort(out, temp, ri, r_size);


  int cli = li, cri = ri;
  for (int i=si; i<si+size; i++) {
    if (cli < li+l_size && cri < ri+r_size) {
      if (out[cli] < out[cri]) {
        temp[i] = out[cli];
        cli++;
      } else if (out[cri] < out[cli]) {
        temp[i] = out[cri];
        cri++;
      } else {
        temp[i+1] = temp[i] = out[cli];
        i++;
        cli++;
        cri++;
      }
    } else if (cli < li+l_size) {
        temp[i] = out[cli];
        cli++;
    } else if (cri < ri+r_size) {
      temp[i] = out[cri];
      cri++;
    } else {
      puts("Error!");
    }
  }

  memcpy(&out[si], &temp[si], sizeof(int)*size);
}

// Generating expected output 
void msort_CPU(int in[], int out[], int N)
{
  memcpy(out, in, sizeof(int) * N);
  int *temp = (int *) malloc(sizeof(int) * N);
  mergesort(out, temp, 0, N);
}


int main(int argc, char** argv)
{

  // specify the dimensions of the input array
  int N = 37;
  if (argc >= 2) {
    N = strtod(argv[1], 0);
  }

  unsigned numbytes = N * sizeof(int);

  int *input = (int *) malloc(numbytes);
  int *exp_out = (int *) malloc(numbytes);
  int *out = (int *) malloc(numbytes);

  fill_array(input, N);
  CPUTimer cputimer;
  cputimer.Start();
  msort_CPU(input, exp_out, N);
  cputimer.Stop();
  printf("The sequential code ran in %f ms\n", cputimer.Elapsed()*1000);
  // print_array(input, N);
  // print_array(exp_out, N);

  int *d_temp, *d_input;

  cudaError_t err;
  cudaMalloc(&d_temp, numbytes);
  cudaMalloc(&d_input, numbytes);

  cudaMemset(d_temp, 0, numbytes);
  cudaMemset(d_input, 0, numbytes);

  cudaMemcpy(d_input, input, numbytes, cudaMemcpyHostToDevice);

  
  GPUTimer timer;
  timer.Start();

  // launching the kernel
  int numThreads = N;
  int M = 32;
  if (argc == 3) {
    M = strtod(argv[2], 0);
  }
  msort<<<numThreads/M+1, M>>>(d_input, d_temp, N);
  // msort<<<1, N>>>(d_input, d_temp, N);

  timer.Stop();
 
  /* Print the last error encountered -- helpful for debugging */
  err = cudaGetLastError();  
  if (err != cudaSuccess)
    printf("Error: %s\n", cudaGetErrorString(err));
  
  cudaMemcpy(out, d_input, numbytes, cudaMemcpyDeviceToHost); 
  print_array(out, N); 
  printf("Result of <msort>: %s\n",compare_arrays(out, exp_out, N) ? "Success" : "Failure"); /* Success <--> correct output */
  printf("The kernel <msort> ran in: %f ms\n", timer.Elapsed());

  return 0; 
}