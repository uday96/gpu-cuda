#include "kernels.h"

__device__ volatile int ctr = 0;

__global__ void histogram(int *d_input, int* d_bin, int M, int N, int BIN_COUNT){
	int index = (blockIdx.x*blockDim.x)+threadIdx.x;
	if(index >= M*N){
		return;
	}
	int bin = d_input[index]%BIN_COUNT;
	atomicAdd(&(d_bin[bin]),(int)1);
}

__global__ void updateBC(int* d_input, int M, int N){
	int index=(blockIdx.x*gridDim.y+blockIdx.y)*(blockDim.x*blockDim.y)+(blockDim.y*threadIdx.x+threadIdx.y);
	int rowID = index/N;
	int colID = index%N;
	if(index >= M*N){
		return;
	}
	if(!(rowID < M && colID < N)){
		return;
	}
	if(rowID == 0 || rowID == M-1 || colID == 0 || colID == N-1){
		d_input[index] = 1;
	}
}

__global__ void stencil(int* d_input, int M, int N){
	int index=(blockIdx.x*gridDim.y+blockIdx.y)*(blockDim.x*blockDim.y)+(blockDim.y*threadIdx.x+threadIdx.y);
	int rowID = index/N;
	int colID = index%N;
	if(index >= M*N){
		return;
	}
	if(!(rowID < M && colID < N)){
		return;
	}
	if(rowID == 0 || rowID == M-1 || colID == 0 || colID == N-1){
		return;
	}
	int tmp = 0.2*(
		d_input[rowID*N+colID]+
		d_input[(rowID+1)*N+colID]+
		d_input[((rowID-1)*N)+colID]+
		d_input[rowID*N+(colID+1)]+
		d_input[rowID*N+(colID-1)]);
	atomicAdd((int *)&ctr,(int)1);
	while(ctr < (M*N)-(2*M)-(2*(N-2)));
	d_input[index] = tmp;
}
