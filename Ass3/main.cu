#include <stdio.h>
#include "kernels.h"
#include <time.h>

#define BIN_COUNT 5

int main(int argc, char** argv){
    int M=100;
    int N=48;
    int K=M*N;
    int input[K],bin[BIN_COUNT]={0},interout[K],interout2[K];
    int binout[BIN_COUNT];
    srand(time(NULL));
    for(int i=0;i<K;i++){
        input[i]=rand()%10;//change
        bin[input[i]%BIN_COUNT]++;
    }
    int *d_input,*d_bin;
    cudaMalloc(&d_input,K*sizeof(int));
    cudaMalloc(&d_bin,BIN_COUNT*sizeof(int));
    cudaMemset(d_bin, 0, BIN_COUNT*sizeof(int));
    cudaMemcpy(d_input, input,K*sizeof(int), cudaMemcpyHostToDevice);

    int x1=4,x2=60;
    histogram<<<10,500>>>(d_input,d_bin,M,N,BIN_COUNT);
    cudaMemcpy(binout, d_bin,BIN_COUNT*sizeof(int),cudaMemcpyDeviceToHost);
    int flag=0;
    for(int i=0;i<BIN_COUNT;i++){
        if(binout[i]!=bin[i]){
            printf("Failure\n");
            flag=1;
            break;
        }
    }
    if(flag!=1){
        printf("Success\n");
    }
    dim3 blocks(5,x1);
    dim3 threads(4,x2);
    cudaMemcpy(d_input, input,K*sizeof(int), cudaMemcpyHostToDevice);
    updateBC<<<blocks,threads>>>(d_input,M,N);
    cudaMemcpy(interout, d_input,K*sizeof(int),cudaMemcpyDeviceToHost);
    flag=0;
    for(int i=0;i<M;i++){
        for(int j=0;j<N;j++){
            if(i==0||j==0||j==N-1||j==M-1){
                if(interout[i*N+j]!=1){
                    printf("Failure\n");
                    flag=1;
                    break;
                }
            }
        }
    }
    if(flag!=1){
        printf("Success\n");
    }
    int flag2=0;
    cudaMemcpy(d_input, interout,K*sizeof(int), cudaMemcpyHostToDevice);
    stencil<<<blocks,threads>>>(d_input,M,N);
    cudaMemcpy(interout2, d_input,K*sizeof(int),cudaMemcpyDeviceToHost);
    if(flag==0){
        for(int x=0;x<M;x++){
            for(int y=0;y<N;y++){
                if(x!=0 && y!=N-1 && x!=M-1 && y!=0){
                    int a=(interout[x*N+y]+interout[(x+1)*N+y]+interout[(x-1)*N+y]+interout[x*N+y+1]+interout[x*N+(y-1)])*0.2;

                    if(interout2[x*N+y]!=a){
                        printf("\n%d %d %d %d\n",x,y,interout2[x*N+y],a);
                        printf("Failure");
                        flag2=1;
                        break;
                    }
                }
            }
        }
        if(flag2==0){
            printf("Success\n");
        }
    }
    else{
        printf("checkSecond\n");
    }
}
