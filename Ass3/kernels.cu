// #include "kernels.h"

// __device__ volatile int ctr = 0;

// __global__ void histogram(int *d_input, int* d_bin, int M, int N, int BIN_COUNT){
// 	int index = (blockIdx.x*blockDim.x)+threadIdx.x;
// 	if(index >= M*N){
// 		return;
// 	}
// 	int bin = d_input[index]%BIN_COUNT;
// 	atomicAdd(&(d_bin[bin]),(int)1);
// }

// __global__ void updateBC(int* d_input, int M, int N){
// 	int index=(blockIdx.x*gridDim.y+blockIdx.y)*(blockDim.x*blockDim.y)+(blockDim.y*threadIdx.x+threadIdx.y);
// 	int rowID = index/N;
// 	int colID = index%N;
// 	if(index >= M*N){
// 		return;
// 	}
// 	if(!(rowID < M && colID < N)){
// 		return;
// 	}
// 	if(rowID == 0 || rowID == M-1 || colID == 0 || colID == N-1){
// 		d_input[index] = 1;
// 	}
// }

// __global__ void stencil(int* d_input, int M, int N){
// 	int index=(blockIdx.x*gridDim.y+blockIdx.y)*(blockDim.x*blockDim.y)+(blockDim.y*threadIdx.x+threadIdx.y);
// 	int rowID = index/N;
// 	int colID = index%N;
// 	if(index >= M*N){
// 		return;
// 	}
// 	if(!(rowID < M && colID < N)){
// 		return;
// 	}
// 	if(rowID == 0 || rowID == M-1 || colID == 0 || colID == N-1){
// 		return;
// 	}
// 	int tmp = 0.2*(
// 		d_input[rowID*N+colID]+
// 		d_input[(rowID+1)*N+colID]+
// 		d_input[((rowID-1)*N)+colID]+
// 		d_input[rowID*N+(colID+1)]+
// 		d_input[rowID*N+(colID-1)]);
// 	atomicAdd((int *)&ctr,(int)1);
// 	while(ctr < (M*N)-(2*M)-(2*(N-2)));
// 	d_input[index] = tmp;
// }

#include <cuda_runtime.h>
#include "kernels.h"

__global__ void histogram(int *d_input, int* d_bin, int M, int N, int BIN_COUNT)
{
		int tid = blockIdx.x * blockDim.x + threadIdx.x; 
		if(tid<M*N)
		{

		int element = d_input[tid] % BIN_COUNT;

		atomicAdd((int *)&d_bin[element], 1);	
		}
}
__global__ void updateBC(int* d_input, int M, int N)
{
	//int t_row = blockIdx.x*blockDim.x+threadIdx.x;
	//int t_col = blockIdx.y*blockDim.y+threadIdx.y;
	
	int uid =  (blockDim.x * blockDim.y) * gridDim.x * blockIdx.y + blockDim.x * threadIdx.y * gridDim.x + blockDim.x * blockIdx.x + threadIdx.x;  
	
	int x = uid / N;
	int y = uid % N;

	if(x< M && y< N )
	{
		if( x==0  ||  y==0  ||  x==M-1  ||  y==N-1)
		{
			d_input[(x*N)+y]=1;
		}
	}
}
__device__ volatile int counter = 0;
__global__ void stencil(int* d_input, int M, int N)
{
	int uid =  (blockDim.x * blockDim.y) * gridDim.x * blockIdx.y + blockDim.x * threadIdx.y * gridDim.x + blockDim.x * blockIdx.x + threadIdx.x;  
	
	int x = uid / N;
	int y = uid % N;

	int c,t,d,l,r;

	 int maxCtr = M+(N-1)+(M-1)+(N-2);


	if(x<M && y<N)
	{
		if( x>0 && y>0 && x<M-1 && y<N-1 )
		{
			 c = d_input[(x*N)+y];
			 t = d_input[(x-1)*N+y];
			 d = d_input[(x+1)*N+y];
			 l = d_input[x*N+(y-1)];
			 r = d_input[x*N+(y+1)];

			 atomicAdd((int *)&counter, 1);
			 
			        while (counter < (M*N)-maxCtr){
			        //Do not do anything until all threads have finished reading
			        }	
			              d_input[(x*N)+y] = 0.2 * (c + t + d + l + r);
		

		}
	}
}